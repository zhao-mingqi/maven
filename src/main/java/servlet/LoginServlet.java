package servlet;

import zut.DO.ResultDO;
import zut.DO.UserDO;
import zut.service.*;
import zut.service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;

@WebServlet(name = "LoginServlet" ,urlPatterns = "/LoginServlet")
public class LoginServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        /*
        * 获得用户输入，进行登录，并根据登录结果跳转到对应界面
        * @Author:Glory
        * @Description:
        * @Date:10:23 2020-11-6 0006
        * @return:
        *
        */
        request.setCharacterEncoding("utf-8");
        String userid = request.getParameter("userid");
        String password = request.getParameter("password");
        response.setCharacterEncoding("gbk");
        UserService userService = new UserServiceImpl();

        ResultDO<UserDO> r = userService.login(userid,password);
        request.getSession().setAttribute("user", r);
        request.getSession().setAttribute("userid", userid);
        request.getRequestDispatcher("AuctionView").forward(request, response);
    }
}
