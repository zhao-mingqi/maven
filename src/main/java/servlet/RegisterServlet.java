package servlet;

import zut.DAO.impl.UserDAOImpl;
import zut.DO.UserDO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "RegisterServlet",urlPatterns = "/RegisterServlet")
public class RegisterServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        /*
        * 根据用户输入进行用户注册
        * @Author:0
        * @Description:
        * @Date:10:26 2020-11-6 0006
        * @return:
        *
        */
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String userid = request.getParameter("userid");
        UserDO user = new UserDO(userid,username,password);
        UserDAOImpl udl = new UserDAOImpl();
        boolean f = udl.insertUserDO(user);
        response.setContentType("text/html;charset=utf-8");
        PrintWriter out = response.getWriter();
        out.println("<!DOCTYPE HTML>");
        out.println("<HTML>");
        out.println("  <HEAD><TITLE>注册结果</TITLE></HEAD>");
        out.println("  <BODY>");
        if(f){
            out.println("<a href='login.html'>注册成功，前往登录</a>");
        }
        else {
            out.println("<a href='register.html'>注册失败，前往注册</a>");
        }
        out.println("  </BODY>");
        out.println("</HTML>");
        out.flush();
        out.close();

    }
}
