package servlet;

import zut.DO.AuctionDO;
import zut.service.impl.AuctionServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "ShopCardController",urlPatterns = "/ShopCardController")
public class ShopCardController extends HttpServlet {
    private static final long serialVersionUID = 1L;
    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        /*
        * 购物车的操作管理
        * @Author:Glory
        * @Description:
        * @Date:10:27 2020-11-6 0006
        * @return:
        *
        */
        request.setCharacterEncoding("gbk");
        Object goodsObject = request.getSession().getAttribute("myGoodsList");
        List<AuctionDO> myGoodsList = null;
        if (goodsObject == null) {
            myGoodsList = new ArrayList<>();
            request.getSession().setAttribute("myGoodsList", myGoodsList);
        } else {
            myGoodsList = (ArrayList<AuctionDO>) goodsObject;
        }
        String type = request.getParameter("type");
        if ("addCard".equals(type)) {
            int id = Integer.parseInt(request.getParameter("id"));
            try{
                int number = Integer.parseInt(request.getParameter("number"));
                AuctionServiceImpl a = new AuctionServiceImpl();
                List<AuctionDO> mylist = a.getallaction();
                mylist.get(id).setNumber(number);
                myGoodsList.add(mylist.get(id));
            }catch (NumberFormatException e){
                response.setContentType("text/html;charset=utf-8");
                PrintWriter out = response.getWriter();
                out.println("<!DOCTYPE HTML>");
                out.println("<HTML>");
                out.println("  <HEAD><TITLE>临时购物车</TITLE></HEAD>");
                out.println("  <BODY>");
                out.println("输入错误，请");
                out.println("<a href='AuctionView'>重新输入</a>");
                out.println("  </BODY>");
                out.println("</HTML>");
                out.flush();
                out.close();
            }
        } else if ("removeCard".equals(type)) {
            int id = Integer.parseInt(request.getParameter("id"));
            myGoodsList.remove(id);
        }
        response.sendRedirect("ShopCard");
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }
}
