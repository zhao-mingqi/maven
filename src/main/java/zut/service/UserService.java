//功能的接口，目前只有login

package zut.service;

import zut.DO.ResultDO;
import zut.DO.UserDO;

public interface UserService {
    ResultDO<UserDO> login(String userid, String password);
}
