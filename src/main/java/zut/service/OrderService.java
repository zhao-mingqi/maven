package zut.service;

import zut.DO.OrderDO;

import java.sql.SQLException;
import java.util.List;

public interface OrderService {
    List<OrderDO> findordersbyid(String userid) throws SQLException;
}
