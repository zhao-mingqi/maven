package zut.service.impl;

import zut.DAO.impl.OrderDAOImpl;
import zut.DO.OrderDO;
import zut.service.OrderService;

import java.sql.SQLException;
import java.util.List;

public class OrderServiceImpl implements OrderService {
    @Override
    public List<OrderDO> findordersbyid(String userid) throws SQLException {
        return new OrderDAOImpl().findallbyid(userid);
    }
}
