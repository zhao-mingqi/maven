package zut.service.impl;

import zut.DAO.UserDAO;
import zut.DAO.impl.UserDAOImpl;
import zut.DO.ResultDO;
import zut.DO.UserDO;
import zut.service.UserService;


public class UserServiceImpl implements UserService {
    private UserDAO userDAO = new UserDAOImpl();
    @Override
    public ResultDO<UserDO> login(String userid, String password) {
        ResultDO<UserDO> r = new ResultDO<>();
        if(password == null || "".equals(password.trim())){
            r.setInfo("用户的密码不能为空");
            return r;
        }
        UserDO user = userDAO.getUserDO(userid);
        if(user !=null){
            if (!user.getPassword().equals(password)){
                r.setInfo("密码不正确");
            }else{
                r.setStatus(true);
                r.setResult(user);
            }
        }else{
            r.setInfo("用户id不存在");
        }
        return r;
    }
}