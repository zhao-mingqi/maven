package zut.DO;

public class AuctionDO {
    /*
    * 商品实体层
    * @Author:Glory
    * @Description:
    * @Date:10:31 2020-11-6 0006
    * @return:
    *
    */
    private String id;
    private String name;
    private double price;
    private String detail;
    private int number;

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public AuctionDO(String id, String name, double price, String detail) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.detail = detail;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public AuctionDO() {
    }
}
