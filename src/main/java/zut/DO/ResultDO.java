//用户的状态，也就是登录的结果
package zut.DO;

public class ResultDO<E> {
    /*
    * 用户状态实体层
    * @Author:Glory
    * @Description:
    * @Date:10:32 2020-11-6 0006
    * @return:
    *
    */
    private String info = null;
    private boolean status = false ;//结果状态，true正常，false不正常
    private E result;//返回的结果对象

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public E getResult() {
        return result;
    }

    public void setResult(E result) {
        this.result = result;
    }
}
