//用户的信息
package zut.DO;

public class UserDO {
    /*
    * 用户实体层
    * @Author:Glory
    * @Description:
    * @Date:10:32 2020-11-6 0006
    * @return:
    *
    */
    private String username;
    private String password;
    private String userid;

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserDO(String userid,  String username,String password) {
        this.username = username;
        this.password = password;
        this.userid = userid;
    }

    public UserDO(String userid, String password) {
        this.username = userid;
        this.password = password;
    }

    public UserDO() {
    }
}
