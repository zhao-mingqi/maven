package zut.DAO;

import zut.DO.AuctionDO;

import java.util.List;

public interface AuctionDAO {
    /*
    * 对商品进行的操作接口
    * @Author:Glory
    * @Description:
    * @Date:10:29 2020-11-6 0006
    * @return:
    *
    */
    List<AuctionDO> getall();

    int insertAcutionDO(List<AuctionDO> auctionDOList);

    boolean insertoneAuctionDO(AuctionDO auction);

    boolean deleteAuctionDO(AuctionDO auction);
}
