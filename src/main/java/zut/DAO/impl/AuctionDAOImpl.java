package zut.DAO.impl;

import zut.DAO.AuctionDAO;
import zut.DO.AuctionDO;
import zut.DO.UserDO;
import zut.util.JDBCUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AuctionDAOImpl implements AuctionDAO {
    @Override
    public List<AuctionDO> getall() {
        Connection connection = JDBCUtil.getConnction();
        String sql = "select * from auction";
        PreparedStatement ps = null;
        try {
            assert connection != null;
            ps =  connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            ArrayList<AuctionDO> list = new ArrayList<>();
            while (rs.next()){
                String auctinid = rs.getString("id");
                String auctinname = rs.getString("name");
                double auctionprice = rs.getDouble("price");
                String auctiondetail = rs.getString("detail");
                list.add(new AuctionDO(auctinid,auctinname,auctionprice,auctiondetail));
            }
            return list;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }
    @Override
    public int insertAcutionDO(List<AuctionDO> auctionDOList) {
        /*
         返回插入商品总数成功的数量
          @Author:Glory
         * @Description:
         * @Date:10:54 2020-10-31 0031
         * @return:

         */
        int count = 0 ;
        for (AuctionDO a:auctionDOList){
            boolean f = insertoneAuctionDO(a);
            if(f){
                count++;
            }
        }
        return count;
    }

    @Override
    public boolean insertoneAuctionDO(AuctionDO auction) {
        /*
        向数据库中添加一个商品信息，返回是否成功
         @Author:Glory
        * @Description:
        * @Date:11:15 2020-10-31 0031
        * @return:

        */
        Connection connection = JDBCUtil.getConnction();
        String sql = "insert into auction values(?,?,?,?)";
        try {
            assert connection != null;
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1,auction.getId());
            ps.setString(2,auction.getName());
            ps.setString(3,Double.toString(auction.getPrice()));
            ps.setString(4,auction.getDetail());
            ps.execute();
            return true;
        } catch (SQLException throwables) {
            return false;
        }
    }

    @Override
    public boolean deleteAuctionDO(AuctionDO auction) {
        Connection connection = JDBCUtil.getConnction();
        String sql = "delete from auction where id = ?";
        try {
            assert connection != null;
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1,auction.getId());
            ps.execute();
            return true;
        } catch (SQLException throwables) {
            return false;
        }
    }
}
