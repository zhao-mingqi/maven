package zut.DAO;

import zut.DO.OrderDO;

import java.sql.SQLException;
import java.util.List;

public interface OrderDAO {
    /*
    * 对订单进行的操作
    * @Author:Glory
    * @Description:
    * @Date:10:30 2020-11-6 0006
    * @return:
    *
    */
    boolean addOrder(OrderDO order);
    //int findorderid(OrderDO order);
    List<OrderDO> findallbyid(String userid) throws SQLException;
    boolean paysuccess(String status ,String userid);
}
