package Test;

import zut.DAO.impl.OrderDAOImpl;
import zut.DAO.impl.UserDAOImpl;
import zut.DO.AuctionDO;
import zut.DO.OrderDO;
import zut.DO.UserDO;
import zut.service.impl.OrderServiceImpl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main {
    public static void main(String[] args) throws SQLException {
        /*
        * 测试用的方法，主要是看bug
        * @Author:Glory
        * @Description:
        * @Date:10:28 2020-11-6 0006
        * @return:
        *
        */
        UserDO user = new UserDO("2018","202001","2018");
        UserDAOImpl udl = new UserDAOImpl();
        boolean f = udl.insertUserDO(user);
        System.out.println(f);
    }
}

